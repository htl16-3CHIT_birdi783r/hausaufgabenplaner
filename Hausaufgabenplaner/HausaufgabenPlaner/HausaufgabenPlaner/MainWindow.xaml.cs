﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HausaufgabenPlaner
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public ObservableCollection<string> Faecher { get; set; }
        public ObservableCollection<Hausaufgabe> Hausaufgabenliste { get; set; }
        public MainWindow()
        {
            Faecher = new ObservableCollection<string>(){
                "SewHausaufgabe","MatheHausaufgabe","DeutschHausaufgabe"
            };
            Hausaufgabenliste = new ObservableCollection<Hausaufgabe>();
            InitializeComponent();
            DataContext = this;


        }



        #region  
        private void cbFaecher_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (cbFaecher.SelectedValue.ToString() == "SewHausaufgabe")
            {
                labelZusatz.Content = "Programmiersprache: ";
                dpZusatz.Visibility = Visibility.Visible;
            }
            else if (cbFaecher.SelectedValue.ToString() == "DeutschHausaufgabe")
            {
                dpZusatz.Visibility = Visibility.Visible;
                labelZusatz.Content = "Textsorte: ";

            }
            else if (cbFaecher.SelectedValue.ToString() == "MatheHausaufgabe")
            {
                dpZusatz.Visibility = Visibility.Visible;
                labelZusatz.Content = "Rechenthema: ";
            }
            else
                dpZusatz.Visibility = Visibility.Collapsed;
        }
        #endregion
        private void butAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                switch (cbFaecher.SelectedIndex)
                {
                    case 0:
                        SewHausaufgabe sew = new SewHausaufgabe(tbAufgabenName.Text, "SewHausaufgabe", listBoxHausaufgaben.Items.Count + 1, dateAbgabe.SelectedDate.ToString().Substring(0, 10), tbBeschreibung.Text, tbZusatz.Text);
                        Hausaufgabenliste.Add(sew);
                        break;
                    case 1:
                        MatheHausaufgabe mathe = new MatheHausaufgabe(tbAufgabenName.Text, "MatheHausaufgabe", listBoxHausaufgaben.Items.Count + 1, dateAbgabe.SelectedDate.ToString().Substring(0, 10), tbBeschreibung.Text, tbZusatz.Text);
                        Hausaufgabenliste.Add(mathe);
                        break;
                    case 2:
                        DeutschHausaufgabe deutsch = new DeutschHausaufgabe(tbAufgabenName.Text, "DeutschHausaufgabe", listBoxHausaufgaben.Items.Count + 1, dateAbgabe.SelectedDate.ToString().Substring(0, 10), tbBeschreibung.Text, tbZusatz.Text);
                        Hausaufgabenliste.Add(deutsch);
                        break;

                    default:
                        throw new NotImplementedException();

                }
            }
            catch
            {
                MessageBox.Show("Kein Gegenstand ausgewählt");
            }
        }

        private void HausaufgabenAnzeigen(object sender, MouseButtonEventArgs e)
        {
            var hausaufgabe = (Hausaufgabe)listBoxHausaufgaben.SelectedItem;

            MessageBox.Show(hausaufgabe.ToString());
        }


        private void butSave(object sender, RoutedEventArgs e)
        {
            string uebergabe = "";
            foreach (var item in Hausaufgabenliste)
            {
                uebergabe += item.ToString() + ";";
            }
            File.WriteAllText(@"data.txt", uebergabe);

        }

        private void butLoad_Click_1(object sender, RoutedEventArgs e)
        {
            string filereadoutput = File.ReadAllText(@"data.txt");
            string[] aufgabensplit = filereadoutput.Split(';');
            for (int i = 0; i < aufgabensplit.Length - 1; i++)
            {
                string[] aufgabenproperties = aufgabensplit[i].Split(',');
                for (int j = 0; j < aufgabenproperties.Length; j++)
                {
                    if (aufgabenproperties[j].Contains("Textsorte"))
                    {
                        DeutschHausaufgabe aufgabe = new DeutschHausaufgabe();
                        foreach (var item in aufgabenproperties)
                        {
                            string[] propertiesLoad = item.Trim().Split(':');
                            if (propertiesLoad[0] == "Aufgabe")
                                aufgabe.ID = listBoxHausaufgaben.Items.Count + 1;
                            else if (propertiesLoad[0] == "Name")
                                aufgabe.Hausaufgabenname = propertiesLoad[1];
                            else if (propertiesLoad[0] == "Gegenstand")
                                aufgabe.Gegenstand = propertiesLoad[1];
                            else if (propertiesLoad[0] == "Textsorte")
                                aufgabe.Textsorte = propertiesLoad[1];
                            else if (propertiesLoad[0] == "Beschreibung")
                                aufgabe.Beschreibung = propertiesLoad[1];
                            else if (propertiesLoad[0] == "Abgabedatum")
                                aufgabe.Abgabedatum = propertiesLoad[1];
                        }

                        //MessageBox.Show(aufgabe.ToString());
                        Hausaufgabenliste.Add(aufgabe);
                    }
                    else if (aufgabenproperties[j].Contains("Programmiersprache"))
                    {

                        SewHausaufgabe aufgabe = new SewHausaufgabe();
                        foreach (var item in aufgabenproperties)
                        {
                            string[] propertiesLoad = item.Trim().Split(':');
                            if (propertiesLoad[0] == "Aufgabe")
                                aufgabe.ID = listBoxHausaufgaben.Items.Count + 1;
                            else if (propertiesLoad[0] == "Name")
                                aufgabe.Hausaufgabenname = propertiesLoad[1];
                            else if (propertiesLoad[0] == "Gegenstand")
                                aufgabe.Gegenstand = propertiesLoad[1];
                            else if (propertiesLoad[0] == "Programmiersprache")
                                aufgabe.Programmiersprache = propertiesLoad[1];
                            else if (propertiesLoad[0] == "Beschreibung")
                                aufgabe.Beschreibung = propertiesLoad[1];
                            else if (propertiesLoad[0] == "Abgabedatum")
                                aufgabe.Abgabedatum = propertiesLoad[1];
                        }
                        //MessageBox.Show(aufgabe.ToString());
                        Hausaufgabenliste.Add(aufgabe);
                    }
                    else if (aufgabenproperties[j].Contains("Rechenthema"))
                    {
                        MatheHausaufgabe aufgabe = new MatheHausaufgabe();
                        foreach (var item in aufgabenproperties)
                        {
                            string[] propertiesLoad = item.Trim().Split(':');
                            if (propertiesLoad[0] == "Aufgabe")
                                aufgabe.ID = listBoxHausaufgaben.Items.Count + 1;
                            else if (propertiesLoad[0] == "Name")
                                aufgabe.Hausaufgabenname = propertiesLoad[1];
                            else if (propertiesLoad[0] == "Gegenstand")
                                aufgabe.Gegenstand = propertiesLoad[1];
                            else if (propertiesLoad[0] == "Programmiersprache")
                                aufgabe.Rechenthema = propertiesLoad[1];
                            else if (propertiesLoad[0] == "Beschreibung")
                                aufgabe.Beschreibung = propertiesLoad[1];
                            else if (propertiesLoad[0] == "Abgabedatum")
                                aufgabe.Abgabedatum = propertiesLoad[1];
                        }
                        //MessageBox.Show(aufgabe.ToString());
                        Hausaufgabenliste.Add(aufgabe);
                    }
                }
            }
        }
    }
}