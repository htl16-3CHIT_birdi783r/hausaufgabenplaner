﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;

namespace HausaufgabenPlaner
{

    public class Hausaufgabe
    {

        public string Hausaufgabenname { get; set; }
        public string Gegenstand { get; set; }
        public int ID { get; set; }
        public string Abgabedatum { get; set; }
        public string Beschreibung { get; set; }

        public Hausaufgabe(string h_name, string gegenstand, int id, string date, string b)
        {
            Hausaufgabenname = h_name;
            Gegenstand = gegenstand;
            ID = id;
            Beschreibung = b;
            Abgabedatum = date;

        }
        public Hausaufgabe()
        {

        }


    }
}
