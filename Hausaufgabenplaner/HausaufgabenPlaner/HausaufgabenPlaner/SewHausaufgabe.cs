﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HausaufgabenPlaner
{
    class SewHausaufgabe : Hausaufgabe
    {
        public string Programmiersprache { get; set; }
        public SewHausaufgabe(string h_name, string gegenstand, int id, string date, string b, string p) : base(h_name, gegenstand, id, date, b)
        {
            Programmiersprache = p;
        }
        public override string ToString()
        {
            /*return "Aufgabe " + ID + ": " + Hausaufgabenname + "\n"
                   + "Gegenstand: " + Gegenstand + "\n"
                   + "Programmiersprache" + Programmiersprache + "\n" 
                   + "Beschreibung: " + Beschreibung + "\n"
                   + "Abgabedatum: " + Abgabedatum.ToString();*/
            return $"Aufgabe: {ID}, Name: {Hausaufgabenname}, Gegenstand: {Gegenstand}, Programmiersprache: {Programmiersprache}, Beschreibung: {Beschreibung}, Abgabedatum: {Abgabedatum.ToString()}";

        }
        public SewHausaufgabe()
        {

        }
    }
}
