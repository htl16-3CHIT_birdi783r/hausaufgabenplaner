﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace HausaufgabenPlaner
{
    class DeutschHausaufgabe : Hausaufgabe
    {
        public string Textsorte { get; set; }
        public DeutschHausaufgabe(string h_name, string gegenstand, int id, string date, string b, string t) : base(h_name, gegenstand, id, date, b)
        {
            Textsorte = t;
        }
        public override string ToString()
        {
            /* return "Aufgabe:" + ID + Hausaufgabenname + "\n"
                    + "Gegenstand: " + Gegenstand + "\n"
                    + "Textsorte: : " + Textsorte + "\n"
                    + "Beschreibung: " + Beschreibung + "\n"
                    + "Abgabedatum: " + Abgabedatum.SelectedDate.ToString();*/

            return $"Aufgabe: {ID}, Name: {Hausaufgabenname}, Gegenstand: {Gegenstand}, Textsorte: {Textsorte}, Beschreibung: {Beschreibung}, Abgabedatum: {Abgabedatum.ToString()}";
        }
        public DeutschHausaufgabe()
        {

        }
    }
}
