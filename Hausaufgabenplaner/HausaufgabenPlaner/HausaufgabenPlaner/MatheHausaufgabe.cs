﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace HausaufgabenPlaner
{
    class MatheHausaufgabe : Hausaufgabe
    {
        public string Rechenthema { get; set; }
        public MatheHausaufgabe(string h_name, string gegenstand, int id, string date, string b, string r) : base(h_name, gegenstand, id, date, b)
        {
            Rechenthema = r;
        }
        public override string ToString()
        {
            /* return "Aufgabe " + ID + ": " + Hausaufgabenname + "\n"
                    + "Gegenstand: " + Gegenstand + "\n"
                    + "Rechenthema: " + Rechenthema + "\n"
                    + "Beschreibung: " + Beschreibung + "\n"
                    + "Abgabedatum: " + Abgabedatum.ToString();*/
            return $"Aufgabe: {ID}, Name: {Hausaufgabenname}, Gegenstand: {Gegenstand}, Rechenthema: {Rechenthema}, Beschreibung: {Beschreibung}, Abgabedatum: {Abgabedatum.ToString()}";

        }
        public MatheHausaufgabe()
        {

        }
    }
}

